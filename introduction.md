# Introduction

L'objectif global de ce cours est de progresser en C++ et d'apprendre la programmation parallèle.

## Organisation

Pour cela, nous disposons de 30 heures divisées en 10 séances, 2 par semaine pendant 5 semaines.
Nous sommes deux intervenants :
- Alexis Pereda <alexis.pereda@ext.uca.fr> ;
- Jérémy Zangla <jeremy.zangla@ext.uca.fr>.

Les premières séances comporteront une partie théorique (*presque* un CM) et une partie pratique accompagnée (TD).
Ces deux parties ne seront pas nécessairement exclusives :
toutes les séances sont effectuées en salles de TP afin de permettre une mise en œuvre immédiate lorsque cela est pertinent.

La seconde partie du cours prendra la forme d'un projet individuel : l'implémentation d'un programme de traitement d'images.
Bien que prévue permettre plus d'autonomie, cette partie ne sera pas exempte de théorie et de pratiques ciblées ponctuelles.

## Environnement

Le développement se fera dans un environnement Linux, en [C++](https://en.cppreference.com/w/) (≤ 17) avec :
- `g++` (le point d'entrée de [GCC](https://gcc.gnu.org/) pour le C++) ;
- Make et [CMake](https://cmake.org/) ;
- [`git`](https://git-scm.com/) ;
- [GitLab](https://gitlab.com/) ;
- `man` (voir `man man`) ;
- [cppreference](https://en.cppreference.com/w/) (ou le `man` de C++).

Liste non exhaustive.
Tous les outils nécessaires seront disponibles sur les ordinateurs mis à disposition.
En cas d'utilisation d'un ordinateur personnel, la charge vous revient d'être compatible avec l'environnement fourni.

## Évaluation

Deux évaluations seront effectuées pour ce cours.
Leur durée ainsi que leur format restent à déterminer exactement.

Cependant, quelques critères sont déjà définis :
- la présence est obligatoire, une absence (injustifiée) peut donc être sanctionnée ;
- la présence **active** est obligatoire.

Ce second point tient notamment compte (donc, *en partie*) de la régularité de votre travail.
Pour cela, un indicateur est la fréquence des `commit` (voir [Git]) poussés sur votre dépôt sur [GitLab](https://gitlab.com/).

## Guide des annotations

- :small_orange_diamond: : indique qu'il peut être intéressant de tester un cas ne fonctionnant pas ;
- :small_blue_diamond: : indique qu'il y a bien plus à dire et que vous êtes invités à poser des questions.
Cela ne veut pas dire qu'il n'y a rien à dire de plus lorsque :small_blue_diamond: n'est pas présent, ni qu'il ne faut pas poser de questions ;
- :arrow_forward: : indique un exercice (et donc _au moins_ un `git commit`).

Lorsqu'une indication est donnée en note en bas de page[^footnote], cela signifie qu'elle vous est donnée à titre informatif et n'est pas une connaissance attendue.

[^footnote]: Comme ceci.
