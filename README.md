# Summary

* [Introduction](introduction.md)
* [Seance n°1 : Base du langage](seance1.md)
* [Seance n°2 : Programmation Orientée Objet](seance2.md)
* [Seance n°3 : Programmation parallèle](seance3.md)
* [Seance n°4 : Conteneurs standards](seance4.md)

* [Complément sur la généricité](gnx.md)