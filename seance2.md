# Seance n°2 : programmation orientée objet

Cette deuxième séance a pour objectif d'aborder la programmation orientée objet tout en commençant le projet.
Nous verrons la syntaxe pour la création d'une classe en C++ avec ses variables et fonctions membres.
Nous verrons également comment modifier la visibilité de ces éléments (*encapsulation*).

Nous utiliserons ici [CMake](https://cmake.org/) pour la première fois.

## CMake

CMake est un générateur de [systèmes de compilation](https://cmake.org/cmake/help/latest/manual/cmake-generators.7.html).
Il permet de n'avoir qu'une seule configuration pour différents systèmes de compilation.
On n'utilisera ici que les makefiles mais CMake pourrait générer une configuration pour [Ninja](https://ninja-build.org/) par exemple (ou même Visual Studio sous Windows).
CMake permet également de gérer plus simplement des projets dans lesquels plusieurs cibles (exécutables ou bibliothèques) sont créées.

Maîtriser CMake n'est pas un objectif de ce cours. Vous devez seulement retenir les commandes qui vous permettent d'utiliser la configuration fournie.

Vous pouvez télécharger la configuration utilisée pour le fil rouge ici : https://gitlab.com/uca_edu/ipcli/-/archive/main/ipcli-main.zip.
L'archive contient un dossier `ipcli-main`, extrayez-le. Vous devez obtenir l'arborescence suivante :

```bash
ipcli-main
├── CMakeLists.txt
├── README.md
├── external
│   ├── CMakeLists.txt
│   └── input.bmp
└── ipcli
    ├── CMakeLists.txt
    ├── cli
    │   ├── CMakeLists.txt
    │   └── cli.cpp
    ├── filters
    │   ├── CMakeLists.txt
    │   ├── conv
    │   │   └── CMakeLists.txt
    │   └── filter.cpp
    ├── ipcli.cpp
    └── tests
        ├── CMakeLists.txt
        └── tests.cpp
```

Dans un terminal, placez-vous dans le dossier `ipcli-main`.
Ce dossier sera considéré comme la racine des futures commandes utilisées.

Si vous utilisez Visual Studio Code, vous pouvez ouvrir ce dossier.
Vous pouvez également y ouvrir un terminal directement depuis VSCode : `barre d'outils` > `Terminal` > `New terminal`.
Si vous êtes placé au bon endroit, la commande `ls` doit vous renvoyer le résultat suivant :

```bash
CMakeLists.txt  README.md  build  external  ipcli
```

### Première configuration

Comme dit précédemment, nous souhaitons que CMake nous génère une configuration pour `make`.
Cette configuration contient donc des *Makefiles* ainsi qu'une multitude d'autres fichiers utilisés par CMake (environ 70 fichiers sont générés dans mon cas).
Pour ne pas polluer l'arborescence du projet, nous allons placer ces fichiers dans un dossier `build`.
Ce dossier est présent dans le `.gitignore`, il ne sera donc pas pris en compte lors de vos commits.

```bash
mkdir build
```

```bash
cd build
```

Pour la suite, on considère que l'on se trouve dans ce dossier `build`.
Demandez à CMake de générer le système de compilation.

```
cmake .. # Ici .. représente le chemin d'accès au dossier contenant le CMakeLists.txt à utiliser pour la configuration.
```

Les fichiers devraient être présents dans le dossier `build` comme prévu. Vous pouvez notemment y appercevoir un `Makefile`.

### Compilation

Vous pouvez ensuite directement compiler et éditer les liens (toujours depuis le dossier `build`).

```bash
make
```

Si le projet compile correctement, votre environnement de développement est prêt pour réaliser le projet. Vous pouvez alors exécuter le programme :

```bash
./ipcli/ipcli
```
Le programme lit une image `input.bmp` se trouvant dans le dossier et produit l'image `output.bmp`.

### Ajouts de fichiers sources

On discute ici de l'ajout de fichiers sources, on ne se trouve donc plus dans le dossier `build` mais dans le dossier racine du projet.

Lorsque vous allez faire grandir le projet, vous allez devoir créer des fichiers.
Il faudra alors indiquer à CMake les fichiers à utiliser.

Pour ce faire, vous devez modifier les fichiers `CMakeLists.txt`.
Plus précisément lorsque vous ajoutez le fichier `monDossier/a.cpp`, vous devez modifier le fichier `monDossier/CMakeLists.txt`.

Les fichiers `CMakeLists.txt` contiennent des variables listant les fichiers sources.
Elles commencent par `_src` pour les fichiers `.cpp` et `_hdr` pour les `.hpp`.
Pour ajouter un fichier, il suffit d'ajouter une ligne avant la fin de la parenthèse et d'y inscrire le nom du fichier.
En mettant un fichier par ligne, vous pouvez ajouter autant de fichiers que nécessaire.

```cmake
set(_src_dossier
    fichier.cpp
    # Ajoutez ici le nom d'un premier fichier
    # Puis un autre fichier
    # Encore un autre
)

set(_hdr_dossier
    fichier.hpp
    # Ici les fichiers .hpp
    # Et encore
    # ...
)
```

Vous pouvez ensuite reprendre les commandes de compilation et d'exécution, l'appel à CMake se fera de manière transparente pour vous.

## Objet

Un objet est un conteneur permettant de rassembler des données sous la forme de variables membres (également appelées attributs) et des comportements avec des fonctions membres (également appelées méthodes).

On définit une classe de cette manière. La définition d'une classe doit se faire dans un fichier d'en-tête :small_blue_diamond: .
Un nom de classe commence par une majuscule.

```cpp
class FirstClass {
};
```

Les variables et fonctions déclarées à l'intérieur de la classe sont dits *membres*. 

:arrow_forward: Ajoutez les fichiers `ipcli/filters/colorScale.cpp` et `ipcli/filters/colorScale.hpp` pour y ajouter la classe `ColorScale` (notre premier filtre) permettant de contenir trois nombres décimaux qui correspondront respectivement à un multiplicateur pour les canaux RGB de chaque pixel de l'image. Initialisez également une variable `colorScaleFilter` de ce nouveau type, dans la fonction `main` (se trouvant dans ipcli/ipcli.cpp).

L'un des fondements du paradigme de programmation orientée objet est l'[**encapsulation**](https://www.larousse.fr/dictionnaires/francais/encapsulation/29088#:~:text=%20encapsulation&text=Opération%20permettant%20d%27enrober%20un,ci%20contre%20les%20influences%20extérieures.).
Ce principe a pour objectif de séparer la manipulation d'un objet (grâce à ses fonctions membres) de sa manière de fonctionner (ses variables membres).
De cette manière l'utilisateur de l'objet n'a pas besoin de se soucier de comment ce dernier a été conçu.
De plus, en accédant directement aux variables membres, l'utilisateur pourrait mettre l'objet dans un état[^objet:etat] invalide.

:arrow_forward: Ajoutez trois fonctions membres permettant d'obtenir les valeurs stockées dans chacun des canaux :small_orange_diamond:.

---

[^objet:etat]: L'état d'un objet est défini par les valeurs de l'ensemble de ses variables membres.

### Visibilité

La visibilité permet de restreindre l'accès à un membre.
Plusieurs niveaux sont disponibles en C++.
La restriction la plus forte est lorsque les membres sont privés (`private`) : les membres ne sont accessibles que depuis la classe.
On peut rendre les membres accessibles depuis l'extérieur de l'objet grâce à la visibilité publique (`public`).

Pour changer la visibilité on procède comme cela :

```cpp
class FirstClass {
private:
    // éléments privés
public:
    // éléments publics    
};
```

Afin de garantir l'encapsulation, on essaiera de toujours déclarer privées les variables membres.
Les fonctions membres permettant d'y accéder auront alors une visiblité plus permissive. (cf. https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#Rc-private [^coreguidelines]).


:arrow_forward: Corrigez la visibilité pour pouvoir accéder aux trois fonctions membres depuis l'extérieur.
Ne vous attardez ici qu'à la compilation du programme et non à son exécution.

Il est possible d'avoir plusieurs sections de même visibilité, et leur ordre n'a pas d'importance :small_orange_diamond:. 

---

[^coreguidelines]: Ce site anglophone référence des bonnes pratiques pour le développement C++.

### Accesseurs

Les accesseurs sont des fonctions membres permettant d'accéder à une partie de l'état d'un objet.

On a déjà fait ici trois accesseurs permettant de lire les valeurs contenues dans les variables membres de `ColorScale`.
Ces accesseurs permettant la lecture d'une valeur sont appelés *getter*.
Leur nom est généralement préfixé de `has` ou `is` lorsqu'on veut connaître la valeur d'un booléen ou de `get` pour tout autre type de données.
Un *getter* est une fonction membre qui souvent ne modifie pas l'objet sur lequel elle est appelée.

:arrow_forward: Rendez la variable `colorScaleFilter` constante :small_orange_diamond:.

Une fonction membre peut être appelée sur une instance[^înstance] constante si et seulement si celle-ci ne modifie pas l'objet.
Pour indiquer au compilateur que cette fonction ne modifie pas l'objet on ajoute `const` après sa liste de paramètres :small_blue_diamond:.

```cpp
class FirstClass {
    float _a;
public:
    float getA() const;
};
```

:arrow_forward: Modifiez les *getter* pour garantir qu'ils ne modifient pas l'instance.

Il est recommandé de mettre les fonctions membres en `const` dès que cela est possible (cf. [CoreGuidelines](https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#Rconst-fct)).

Le second type d'accesseur est appelé *setter*.
Ils permettent de modifier une variable membre depuis l'extérieur et sont préfixés par `set`.

:arrow_forward: Ajoutez un setter par canal de couleur :small_orange_diamond:.

---

[^înstance]: Une instance de classe est une variable dont les caractéristiques et le comportement sont définis par une classe.

## Initialisation et durée de vie des objets

### Initialisation

:arrow_forward: Lors de l'exécution, quel est l'état de la variable `colorScaleFilter`. :small_blue_diamond:

Pour donner l'état d'un objet à sa création, on doit donner une valeur à ses variables membres :small_blue_diamond:.
Pour ce faire, on crée une fonction membre spéciale qui permet d'indiquer comment l'objet doit être construit.

Le *constructeur* possède le même nom que la classe et n'a pas de type de retour.

```cpp
class FirstClass {
    FirstClass();
};
```

Pour initialiser des variables membres dans un constructeur, on utilise la liste d'initialisation. Les variables membres qui y sont initialisées doivent l'être dans l'ordre où elles ont été déclarées :small_orange_diamond:.

```cpp
class FirstClass {
    int _a;
    int _b;
public:
    FirstClass(int a, int b);
};

FirstClass::FirstClass(int a, int b):
    _a(a),
    _b(b)
{}
```

:arrow_forward: Ajouter un constructeur avec trois paramètres afin d'initialiser les variables membres de `ColorScale` :small_orange_diamond:.

### Destruction

Certains objets ont besoin de manipuler des ressources plus complexes.
Un exemple simple de ressource est de la mémoire allouée manuellement et accessible via un pointeur.
Rappelez-vous que vous devez éviter au maximum de faire ces allocations.

Ces ressources sont initialisées et manipulées pendant la vie de l'objet mais leur libération n'est pas automatique.
Vous devez donc indiquer au compilateur quel comportement suivre pour libérer correctement les ressources.

Le destructeur de la `FirstClass` est la fonction membre suivante :

```cpp
~Firstclass() {
    // Libération des ressources.
}
```

On ne devrait pas avoir besoin de manipuler les destructeurs dans le cadre du fil rouge.

### Ordre de construction et de destruction

Mise à jour : Le concept de destructeur a été expliqué un peu plus haut pour rendre la page plus cohérente.

Les variables sont créées dans l'ordre de leur déclaration.
Elles sont ensuite en vie jusqu'à atteindre la fin de leur portée.[^scope]
Les variables ayant atteint la fin de leur portée (*scope* en anglais) sont détruites dans l'ordre inverse de leur création.

:arrow_forward: Testez la durée de vie des objets en écrivant du texte à la création et à la destruction d'objets. 

[^scope]: La [portée](https://en.cppreference.com/w/cpp/language/scope) d'un identifiant (comme une variable par exemple) est la portion de code dans laquelle la variable est valide. Pour faire (trop) simple, un scope est délimité par une paire d'accolades.

## La bibliothèque CImg

On utilise, dans le cadre du projet, la bibliothèque [CImg](https://cimg.eu/) qui permet la manipulation d'images en C++. Seules quelques unes de ses fonctionnalités nous intéressent : manipulation des pixels, interopérabilité des formats d'images.

:arrow_forward: Ajoutez à la classe `ColorScale` une fonction membre `apply` prenant en paramètre une image (le type est indiqué dans la fonction `main` donnée). On modifiera alors chaque pixel de l'image pour appliquer les multiplicateurs aux canaux de couleurs. L'image passée en paramètre sera modifée directement, la fonction ne renvoie donc aucune valeur.

## Composition

On a pour le moment simplement conservé des types fondamentaux dans nos objets. On peut aussi contenir des objets dans des objets.

:arrow_forward: Définissez deux objets `A` et `B`, chacun affichant sur la sortie standard un message à la création et à la destruction. Ajoutez une variable membre de type `A` dans la classe `B`. Vérifiez la durée de vie de ces objets. 

## Héritage

Lorsque plusieurs objets doivent posséder un comportement semblable, il est préférable de rassembler ce dernier dans un objet servant de **Base**.
On peut ensuite indiquer dans chacun des objets **dérivés** leur comportement spécifique (c'est-à-dire qui diffère des autres).

On trouve souvent les termes de classe **mère** et classe **fille** qui correspondent respectivement à **base** et **dérivée**.

L'exemple suivant permet d'illustrer cet aspect simplement. 
On définit une classe mère `Artiste` et des classes filles `Peintre`, `Sculteur`, ...
La classe mère peut ainsi gérer le comportement commun :
 - variables membres : `nom`, `age` ;
 - fonctions membres : accesseurs pour ces variables, `faireUneExposition()`.

Pour faire hériter une classe `F` d'une classe `M` on utilise la syntaxe suivante :

```cpp
class F : public M {
    // Définition classique d'une classe.
};
```

Dans l'exemple précédent, le terme `public` permet d'indiquer une restriction de la visibilité des membres de la classe `M` dans la classe `F`.
Pour ce cours, on ne s'attardera pas plus sur cet aspect. On indiquera donc toujours `public` dans le cadre d'un héritage. 

<details><summary>Un peu plus d'informations sur la visibilité de l'héritage</summary>

La classe `M` définit ici plusieurs variables membres.

```cpp
class M {
private:
    int _private;
protected:
    int _protected;
public:
    int _public;
};

class F_pub : public M {
    /*
     * L'héritage est publique, c'est la visibilité la plus large. 
     * Il n'y a donc aucune contrainte supplémentaire sur les membres de M.
     */
};
class F_prot : protected M {
    /*
     * L'héritage est protégé. Il y a donc une contrainte supplémentaire sur les membres de M.
     * En l'occurence le seul membre dont la visibilité est modifiée est _public qui sera désormais protégé depuis cette classe.
     */
};
class F_priv : private M {
    /*
     * L'héritage est privé, c'est la visibilité la plus contraignante.
     * Tout devient donc privé, _protected et _public seront considérés privés.
     */
};
```

Pour manipuler et tester cet héritage, on peut s'attarder sur les erreurs de compilation générées ici (attention à inclure correctement les classes définies précédemment):

```cpp
#include <iostream>

int main() {
    M m;
    F_priv f;

    std::cout << m._public << std::endl;
    std::cout << f._public << std::endl; // Erreur ici : ‘int M::_private’ is private within this context
}
```

Dans cette exemple, des variables membres ont été utilisées.
Ce comportement est identique pour les fonctions membres.

</details>

### Polymorphisme

Une fois la relation d'héritage établie, il est possible d'utiliser des classes filles comme si elles étaient des classes mères.

```cpp
void f1(Mere & m);

int main() {
    Mere m;
    Fille f;

    f1(m); // m est bien de type `Mere`.
    f1(f); // Fille hérite publiquement de Mere, on peut donc l'utiliser lorsque l'on a besoin de manipuler une variable de type `Mere`.
}
```

<details><summary>Troncature de type</summary>
Lorsque l'on manipule des objets hérités il faut faire attention aux copies.
En faisant la copie d'une variable fille dans une nouvelle variable de type mère, on tronque le type.
La variable résultante de la copie perd l'information de son type d'origine.

```cpp
int main() {
    Fille f;
    Mere m = f; // m perd l'information qu'elle provient d'une variable de type `Fille`.
}
```

Attention aux copies lors du passage de paramètre dans les fonctions.
</details>

:arrow_forward: Ajoutez une classe `Filtre` au projet, cette classe possède la fonction `apply` vue pour `ColorScale`. L'implémentation de la fonction `apply` ne contiendra rien dans la classe `Filtre`. La classe `ColorScale` hérite de `Filtre` et donne sa propre implémentation de la fonction `apply`.

:arrow_forward: En évitant la troncature de type, vérifiez quelle implémentation de la méthode `apply` est appelée lorsqu'on manipule une variable de type `Filtre&` mais initialisée à partir d'un `ColorScale` :small_orange_diamond:.

### Redéfinition des fonctions membres

Le polymorphisme donne également la possibilité aux classes filles de redéfinir les méthodes de la classe mère, c'est l'*overriding*.
Cela corrige le problème vu précédemment.

Pour chacune des fonctions membres dont le comportement est redéfini dans les classes filles, il faut ajouter le spécificateur `virtual`.
Ce spécificateur se rajoute avant le type de retour et uniquement au sein de la définition de la classe.

```cpp
// Fichier d'entête
class Mere {
public:
    virtual void f();
};


// Fichier d'implémentation
void Mere::f() {
}
```

Les classes filles n'ont pas besoin d'indiquer le `virtual` lorsqu'elles définissent les fonctions pour les redéfinir.
Cette transitivité de spécificateur permet à une classe dérivée d'une classe dérivée de redéfinir elle aussi une fonction membre.

```cpp 
class Mere {
public:
    virtual void f();
};

class Fille : public Mere {
    void f() override; // Pas besoin de spécifier le `virtual` pour que Fille2 puisse la redéfinir.
};

class Fille2 : public Fille {
    void f() override;
}
```

Les redéfinitions des fonctions contiennent le spécificateur `override`.
Celui-ci est facultatif, mais on l'indiquera systématiquement lorsqu'on redéfinit une fonction d'une classe mère. 

<details><summary>vtable</summary>
La *vtable* pour *Virtual Table* est un tableau généré par le compilateur permettant au programme de décider lors de l'exécution quelle implémentation de la fonction membre il faut appeler.
En ajoutant `virtual` sur une fonction d'une classe mère, chaque redéfinition de la fonction membre sera indiquée dans la vtable.
Le programme pourra alors vérifier le type exact de la variable lors de l'exécution afin d'aller chercher la bonne fonction dans ce tableau.

Lorsque `virtual` n'est pas précisé la vtable n'est pas générée. Le compilateur se contente d'exécuter la fonction du type qu'il connait lors de la compilation.
</details>

Lorsqu'une classe mère possède des fonctions membres virtuelles, il faut obligatoirement rendre son destructeur virtuel également.
On doit donc définir le destructeur dans la classe mère, même si il ne fait rien. 
Les classes filles sont libres de redéfinir ou nom le destructeur selon leur besoin.

```cpp 
class Mere {
public:
    virtual ~Mere(){}
    virtual void f();
};

class Fille : public Mere {
public:
    ~Fille() override { // On peut (doit) également ajouté le spécificateur `override` pour les destructeurs
        /* Libère des ressources allouées */
    }
    void f() override; // Pas besoin de spécifier le `virtual` pour que Fille2 puisse la redéfinir.
};

class Fille2 : public Fille {
public:
    // Ne redéfinit pas le destructeur comme elle ne gère aucunes ressources supplémentaires.
    void f() override;
}
```
