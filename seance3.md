# Séance n°4 : programmation parallèle

La puissance des ordinateurs ne croît plus tellement en fréquence pure des processeurs.
À la place, les ordinateurs disposent de plus en plus de processeurs.
Pour en tirer profit, il est donc nécessaire d'écrire des programmes qui utilisent activement ces différents processeurs, on parle alors de programmation parallèle.

La programmation parallèle peut être accomplie de différentes manières.
Celle qui sera abordée dans ce cours est la programmation multithread.

Cette séance abordera les moyens techniques pour concevoir un programme parallèle, les problématiques induites et leur solution théorique et pratique.

## Un peu de théorie

Un *thread* (terme anglais dont les traductions françaises, processus léger, fil, ..., sont moins fréquemment employées)
se comporte essentiellement de la même manière qu'un processus.
Les *threads* en revanche partagent la majorité de la mémoire du processus auquel ils appartiennent.
La pile fait exception (donc en particulier, les variables locales d'une fonction exécutée dans un *thread* sont propres à celui-ci).

Pour l'ordonnanceur, un *thread* est traité comme un processus, et son cycle de vie est présenté dans la figure suivante.

<div align="center"><img src="https://phd.pereda.fr/assets/par/thread_life_cycle.png" width="600"></div>

Après sa création, un *thread* est prêt à s'exécuter.
L'ordonnanceur doit décider, selon un algorithme, quel *thread* parmi tous ceux qui sont prêts doit s'exécuter,
c'est-à-dire occuper du temps du processeur.
Lorsqu'un *thread* est élu, il dispose du processeur, généralement pour un temps donné, lorsque l'ordonnanceur est dit « préemptif », ce que l'on va considérer vrai dans notre cas.
Une fois ce temps écoulé, l'ordonnanceur préempte le *thread* qui retourne à l'état prêt.

Pendant son exécution, un *thread* peut avoir besoin d'une ressource dont l'accès est bloquant lorsqu'elle est indisponible.
Par exemple, la lecture d'une entrée utilisateur, d'un message réseau, ...
Le système repasse le *thread* à l'état prêt dès que la ressource devient disponible.
Enfin, l'exécution d'un *thread* peut s'achever.

On notera en particulier que rien ne contraint la machine qui exécute cela de disposer de multiple processeurs.
Sur une machine n'ayant qu'un processeur, ce mécanisme permet seulement de rendre indépendantes différentes tâches.
C'est un concept très utile et que l'on appelle la concurrence.

Si la machine dispose de plusieurs processeurs, autant de *threads* peuvent être simultanément en cours d'exécution.
Notre objectif repose sur cette contrainte puisqu'il s'agit de diminuer le temps total d'exécution d'un programme.

Une mesure basique du gain obtenu s'appelle l'accélération (en anglais, *speedup*) et correspond au rapport du temps d'exécution séquentiel sur le temps d'exécution parallèle.
Ainsi, avec $`t_s`$ le temps moyen d'une exécution séquentielle et $`t_p(n)`$ le temps moyen d'une exécution parallèle avec $`n`$ cœurs, on a :
```math
a(n) = \frac{t_s}{t_p(n)}
```
l'accélération pour $`n`$ cœurs.

Une remarque faite par Amdahl est que cette accélération est limitée par la proportion séquentielle d'une programme.
Ainsi, si on réduit un programme à cette idée qu'il y a $`f \in [0, 1]`$ la fraction parallélisable, et donc $`1-f`$ la fraction obligatoirement exécutée séquentiellement, on a
```math
a_f(n) = \frac{1}{(1-f) + \frac{f}{n}}
```

Et en particulier, puisque l'on considère que le nombre de cœurs est essentiellement illimité, on a
```math
\lim_{n \to \infty} a_f(n) = \frac{1}{1-f}
```

Et donc, si le programme ne peut pas du tout être parallélisé, l'accélération maximale atteignable est trivialement de 1, indépendamment du nombre de cœurs accessibles.
L'intérêt de cette formule (qui possède certains défauts :small_blue_diamond:) est de mettre en valeur l'importance des parties non parallélisables d'un programme
sur les limites que cela induit sur les performances générales que l'on peut espérer atteindre.
Ce point en particulier aura son importance par la suite.

## Un peu de pratique

### Création et terminaison d'un *thread*

Créer un *thread* en C++ se fait en créant une instance de `std::thread` construite avec une fonction a exécuter.

```cpp
#include <iostream>
#include <thread>

void f() {
	std::cout << "traitement exécuté dans un second thread\n";
}

int main() {
	std::thread thread(f);

	std::cout << "traitement exécuté dans le thread principal\n";

	thread.join(); // attente de la terminaison du thread
}
```

En théorie, l'ordre d'affichage est imprédictible puisque les deux *threads* sont ordonnancés indépendamment.
Cependant, dans cet exemple, il est extrêmement probable de voir les messages dans l'ordre second *thread* puis *thread principal* en raison de la durée des tâches effectuées relativement au temps nécessaire à la création d'un *thread*.

On utilise souvent des fonctions lambda comme argument du constructeur de `std::thread`, ce qui, pour l'exemple ci-avant, peut s'écrire ainsi :

```cpp
#include <iostream>
#include <thread>

int main() {
	std::thread thread([] {
		std::cout << "traitement exécuté dans un second thread\n";
	});

	std::cout << "traitement exécuté dans le thread principal\n";

	thread.join();
}
```

<details><summary>CMakeLists.txt</summary>

```
cmake_minimum_required(VERSION 3.10)

project(parallel)

find_package(Threads REQUIRED)

add_executable(example src/main.cpp)
target_compile_options(example PRIVATE -Wall -Wextra -Wfatal-errors -pthread)
target_link_libraries(example PRIVATE Threads::Threads)
```
</details>

La terminaison d'un *thread* doit être attendue par le *thread* qui l'a créé, ce que fait la fonction membre `std::thread::join`.
C'est une forme de synchronisation puisque la fonction `std::thread::join` ne se termine que lorsque le *thread* est lui-même terminé.

### Partage de mémoire

Un *thread*, au contraire d'un processus, partage sa mémoire (précisément, celle du processus auquel il appartient) avec les autres *thread* de ce processus.
Cela signifie en particulier que l'on peut écrire par exemple :

```cpp
int main() {
	unsigned shared = 0;

	auto task = [](unsigned& shared) {
		for(int i = 0; i < 1'000'000; ++i) ++shared;
	};

	std::array<std::thread, 10> array;
	for(auto it = std::begin(array); it != std::end(array); ++it)
		*it = std::thread(task, std::ref(shared));

	for(auto& thread: array) thread.join();

	std::cout << "total: " << shared << '\n';
}
```

Dans ce code, on crée un tableau de 10 *threads* qui exécutent tous la même tâche.
Pour donner une référence sur `shared`, à cause de l'implémentation du constructeur de `std::thread`, on doit indiquer qu'il faut que cela reste une référence.
Attention : lorsqu'une fonction prend une référence en paramètre, on ne doit pas utiliser `std::ref` !
Il s'agit ici d'un cas très particulier.

<details><summary>Écriture alternative du même programme</summary>
Une autre manière d'écrire le code ci-avant, si l'on sait que les fonctions lambda peuvent *capturer* des variables, est la suivante :

```cpp
int main() {
	unsigned shared = 0;

	auto task = [&]{ for(int i = 0; i < 1'000'000; ++i) ++shared; };

	std::array<std::thread, 10> array;
	std::generate(std::begin(array), std::end(array), [&]{ return std::thread(task); });

	for(auto& thread: array) thread.join();

	std::cout << "total: " << shared << '\n';
}
```

Entre les crochets peut être indiquée une liste de variables dont l'accès doit être possible au sein de la lambda.
Indiquer seulement `&` fait que toute variable y sera accessible dès lors qu'elle y est utilisée.
La fonction `std::generate` est un *algorithme* de la bibliothèque standard.
Cet algorithme permet d'exécuter une fonction pour chaque élément d'une plage donnée (entre `std::begin` et `std::end` dans notre cas, donc toute la collection).
Le résultat de cette fonction est utilisé pour donner une valeur à l'élément correspondant de la collection.
En bref, on crée un *thread* exécutant la tâche `task` pour chaque case du tableau.
</details>

:arrow_forward: Quel résultat attendez-vous à l'exécution de ce programme ?

:arrow_forward: Exécutez le programme, comparez. Que vous ayez eu raison ou non, réfléchissez à la raison de ce résultat.

Dans le CMakeLists.txt, on peut ajouter l'option `-fsanitize=thread` (option de compilation **et** d'éditions des liens).

:arrow_forward: Exécutez à nouveau le programme après l'avoir construit à nouveau avec cette option.

## Les sémaphores, c'est mon fort

Le partage de ressources fait que le programme ne se comporte plus comme désiré.
Pour pallier ce problème, il existe les sémaphores.

Essentiellement, un sémaphore est un compteur (strictement positif) associé à une file d'attente.
Il existe deux opérations principales :
- prendre (que l'on notera P) ;
- libérer (que l'on notera V).

L'opération P :
- si le compteur du sémaphore est non nul, le décrémente ;
- sinon, bloque le *thread* et l'ajoute en file d'attente.

L'opération V :
- s'il y a au moins un *thread* en file d'attente, l'en sort et place le *thread* à l'état prêt ;
- sinon, incrémente le compteur du sémaphore.

Ces opérations sont gérées par le système d'exploitation et ont la particularité d'être effectuées de manière atomique du point de vue de l'ordonnanceur.
C'est une propriété fondamentale sans laquelle les sémaphores ne peuvent fonctionner (car il faudrait des sémaphores pour les protéger...).

Pour illustrer le fonctionnement, prenons deux *threads* concurrents partageant le sémaphore `s` initialisé à 0 :

*Thread* A :
```
P(s)
// tâche A
```

*Thread* B :
```
// tâche B
V(s)
```

Étant donné le fonctionnement des sémaphores...

:arrow_forward: Convainquez-vous que peu importe dans quel ordre sont exécutées les instructions des deux *threads*, l'ordre des tâches A et B est garanti.

:arrow_forward: Dans quel ordre sont exécutées les tâches A et B ?

:arrow_forward: Si on avait initialisé le sémaphore à 1 plutôt qu'à 0, cela changerait-il l'exécution ?


Si on remplace les *threads* pour que leur code soit exécuté en boucle, ainsi, sachant que le sémaphore est initialisé à 0 :

*Thread* A :
```
loop {
	P(s)
	// tâche A
}
```

*Thread* B :
```
loop {
	// tâche B
	V(s)
}
```

:arrow_forward: Que se passe-t-il ?

<details><summary>Exercices à dévoiler après avoir répondu au précédent</summary>
:arrow_forward: On veut une alternance des tâches A et B, comment faire ?

:arrow_forward: Si l'on considère les tâches A et B indépendantes (on peut les exécuter dans n'importe quel ordre, ...)
mais elles utilisent toutes deux une ressource commune qui ne doit pas être utilisée simultanément, que faut-il faire ?
</details>

Enfin, considérons un cas un peu plus complexe : on dispose de $`n`$ *threads* « producteurs » et d'un *thread* qui doit poursuivre son exécution seulement
après la terminaison des producteurs.

*Threads* producteurs :
```
// production...
```

*Thread* consommateur :
```
// utilisation des produits
```

:arrow_forward: En utilisant des sémaphores, garantissez que les $`n`$ productions, qui sont effectuées en parallèle, seront terminées avant que l'utilisation des produits côté consommateur ne s'exécute.

## Abstraction des sémaphores

En C++, on ne manipule en général pas les sémaphores directement.
D'abord parce que leur implémentation dépend du système d'exploitation (on perd en portabilité).
Ensuite, parce qu'en utilisant des abstractions, on peut augmenter l'expressivité du code.

Dans notre dernier exercice, on a écrit une barrière de synchronisation extrêmement simple en utilisant un sémaphore.
Il se trouve que ce concept est naturellement implémenté par `std::thread::join` lorsque les *threads* producteurs se terminent après avoir produit.
Ainsi, dans les premiers exemples en C++ manipulant les *threads*, dès lors qu'un tableau de *threads* a été utilisé, nous étions précisément dans ce cas.

De manière plus générale, en revanche, lorsqu'un *thread* peut produire sans s'arrêter immédiatement après, on ne peut pas recourir à `std::thread::join`.
Il existe une notion qui répond au problème, les `std::condition_variable`.
Ceci ne sera pas abordé pour ce cours.

Dans la section précédente, nous avons vu un cas très particulier d'utilisation des sémaphores : un sémaphore initialisé à 1, et utilisé systématiquement avec d'abord une opération « P », puis une opération « V ».
Cela permet d'introduire une section d'exclusion mutuelle, puisqu'un seul *thread* peut se trouver entre le « P » et le « V » à un instant donné.
En anglais, « exclusion mutuelle » devient « mutual exclusion », raccourci en « mutex ».
Le C++ fournit un type, `std::mutex`, lequel nous permet deux opérations qui rappelleront « P » et « V », mais plus expressivement :
- `std::mutex::lock` ;
- `std::mutex::unlock`.

:arrow_forward: Modifiez le code dysfonctionnel en introduisant un `std::mutex` partagé.

Un développeur C++ n'utilisera en principe pas ces fonctions : dans de nombreux cas, cela introduit des tests supplémentaires.
Le concept de « RAII » en C++ fait que la bibliothèque standard peut fournir un outil pour appeler ces fonction de manière plus sûre.
Cet outil s'appelle `std::lock_guard`.

On peut ainsi écrire :

```cpp
std::mutex m;

{
	std::lock_guard<std::mutex> lg(m);
	// section d'exclusion mutuelle
}
```

L'opération `std::mutex::lock` est effectuée par le constructeur, et `std::mutex::unlock` est appelé par le destructeur de `lg`.

:arrow_forward: Modifiez à nouveau votre code pour utiliser `std::lock_guard`.

Rappelez-vous des formules d'Amdahl, citées en début de séance.

:arrow_forward: Quelles implications ont cette formule sur l'utilisation des mutex ?
