## La généricité

Si le C++ permet la programmation orientée objet, il ne se limite pour autant pas à ce seul paradigme.
D'une manière très générale, un programme traite des données pour produire un résultat, et ce traitement est spécifique.
Par exemple, si l'on doit incrémenter de $`7`$ un nombre, on peut écrire la fonction $`f(x) = x+7`$.
Cependant, un traitement spécifique peut être généralisé : si l'on fournit $`add(l, r) = l+r`$, on peut ensuite implémenter $`f`$ en utilisant $`add`$
(c'est d'ailleurs en réalité précisément ce que l'on a fait initialement en utilisant l'opérateur mathématique $`+`$).
La conception de bibliothèque repose fondamentalement sur ce principe de généralisation des traitements afin de partager ces fragments communs de code entre différentes fonctions,
et plus globalement entre différents projets.

Cet exemple peut être lui-même généralisé pour obtenir ce à quoi correspond le paradigme qu'est la généricité.
En C++, la généricité repose sur les patrons, de l'anglais *template*, terme qui sera utilisé dorénavant.
L'idée générale d'un template est de décrire un fragment de code avec des inconnues.
Ceux-ci peuvent être :
- des types (classes) ;
- des fonctions ;
- des variables.

Quant aux inconnues (appelées variables template) qui peuvent être utilisées, il s'agit :
- de valeurs (entières) ;
- de types ;
- et même d'autres templates.

Dans cette introduction minimale à la généricité, nous nous contenterons de voir comment cela se passe pour les classes et les fonctions.
Nous utiliseront uniquement des valeurs entières et des types comme variables template.

### Template de fonctions avec paramètres template entiers

Dans un premier temps, afin de découvrir la syntaxe, voyons un exemple concret de template de fonction prenant des paramètres template entiers.

```cpp
template<int l, int r>
int sum() {
	return l+r;
}
```

La ligne `template<int l, int r>` débute un template, et la liste de ses paramètres est placée entre chevrons plutôt qu'entre parenthèses pour le distinguer d'une fonction « classique ».
L'identifiant `sum` est alors un template de fonction et peut être utilisé ainsi :

```cpp
sum<5, 7>
```

Attention : l'instanciation de ce template (on appelle l'appel avec les chevrons une instanciation) produit une fonction (puisqu'il s'agit d'un template de... fonction).
Le résultat n'est donc pas la somme de ses arguments mais bien la fonction qui retourne cette somme.
Si l'on décide de conserver cette fonction dans une variable :

```cpp
auto f = sum<5, 7>;
```

On peut alors utiliser cette fonction normalement, elle accepte exactement zéro paramètre et retourne directement le nombre $`12`$.
Pour légèrement complexifier cet exemple, on peut garder un des deux paramètres au niveau de la fonction générée :

```cpp
template<int l>
int sum2(int r) {
	return l+r;
}
```

En instanciant ce second template de fonction, par exemple avec `sum2<5>` (on note que le template ne prend qu'un paramètre dans ce second exemple),
on obtient une fonction qui, cette fois, accepte un paramètre et ressemble à :

```cpp
int sum2__5(int r) {
	return 5+r;
}
```

### Template avec types en paramètres template

Comme dit dans l'introduction de cette section, on peut également utiliser des types comme paramètres template d'un template.
L'illustration de l'intérêt de la généricité est assez facile dès lors que l'on utilise au moins un type comme paramètre template.

Par exemple, dans certains langages, on peut définir ces fonctions :

```cpp
int min_int(int l, int r) {
	return l < r? l : r;
}

float min_float(float l, float r) {
	return l < r? l : r;
}

double min_double(double l, double r) {
	return l < r? l : r;
}
```

En C++, la surcharge des fonctions nous permet d'améliorer légèrement ceci en autorisant la réutilisation d'un unique nom pour toutes les versions selon le type des paramètres.
En revanche, un œil minimalement attentif observera que le code de chacune des implémentations est assez... similaire, indépendamment du type des variables.
Grâce à la généricité, et donc en C++ aux templates, on peut écrire :

```cpp
template<typename T>
T min(T l, T r) {
	return l < r? l : r;
}
```

Le mot-clé `typename` permet d'indiquer que le nom qui suit doit correspondre à un nom de type.
On peut ensuite utiliser ce template de fonction ainsi `min<int>(5, 7)`.
En pratique, le C++ dispose de nombreux outils pour faciliter la programmation générique et son utilisation, ainsi dans ce cas précis, on peut en réalité écrire `min(5, 7)`.
Le compilateur se charge alors de déduire le type auquel doit correspondre `T`.

### La bibliothèque standard

La bibliothèque standard utilise extensivement la généricité.
En particulier, tous les conteneurs standards sont en réalité des templates de classes, d'où par exemple `std::vector<int>`.
En effet, `std::vector` est un template de classe acceptant un type comme paramètre template, type ensuite utilisé pour les valeurs à conserver au sein du vecteur.
