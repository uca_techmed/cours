# Séance n°4 : conteneurs standards

Pour terminer le cours, nous allons aborder quelques structures de données.
Nous verrons également leur manipulation en C++ au travers des implémentations faites dans la bibliothèque standard.
Des liens seront présents pour vous renvoyer sur https://en.cppreference.com où vous pourrez trouver plus d'informations sur les conteneurs (*Containers library*).

## Structures de données

Une structure de donnée a pour objectif d'organiser et de rassembler des données similaires afin de simplifier leur manipulation.

L'implémentation dans la bibliothèque standard du C++ de ces structures de données utilise la généricité. 
Ce sujet étant vaste, nous avons décidé de ne pas l'intégrer directement dans ce cours.
Vous pouvez trouver un complément à ce sujet [ici](gnx.md)).

### Tableaux 

Les tableaux sont des conteneurs simples stockant de manière contiguë des éléments de même type.
On accède à un élément à partir de son indice.
La représentation en mémoire d'une telle structure ressemble à [cela](img/vector_a.png) :

<div align="center"><img src="img/vector_a.png" width="400"></div>

En C++, on utilise [`std::vector<T>`](https://en.cppreference.com/w/cpp/container/vector) pour contenir les données sous la forme d'un tableau. On indique à la place de `T` le type que doit contenir de le tableau.
L'exemple suivant instancie un tableau `tab` de taille 3 et contenant des entiers :

```cpp
#include <vector>

int main() {
    std::vector<int> tab(3);
}
```

<details><summary>Constructeurs de `std::vector`</summary>

Vous pouvez initialiser les valeurs d'un vecteur directement, mais la syntaxe peut parfois apporter de la confusion avec la construction à une taille précise.
Attention donc à bien choisir entre `{}` et `()`.

```cpp
#include <iostream>
#include <vector>

int main() {
    std::vector<int> vec1(5); // Taille de 5 (tous les éléments sont construits par défaut : ici 0)
    std::vector<int> vec2({5}); // Taille de 1 avec l'élément 5
    std::vector<int> vec3{5}; // Taille de 1 avec l'élément 5
    std::vector<int> vec4({1, 2, 3, 4, 5}); // Taille de 5 avec les éléments [1; 5]
    std::vector<int> vec5{1, 2, 3, 4, 5}; // Taille de 5 avec les éléments [1; 5]
}
```
</details>

On peut accéder à un élément en utilisant l'opérateur `[]`.
Cet accès permet la lecture et l'écriture :

```cpp
std::cout << tab[1] << '\n';
tab[1] = 2;
```

Le `std::vector` est un tableau dont la capacité n'est pas fixe.

<details><summary>Différence entre capacité et taille</summary>

En structure de données, il ne faut pas confondre ces deux valeurs.
La première, **capacité**, est le nombre d'éléments que l'on peut stocker au maximum.
La seconde, **taille**, est le nombre d'éléments actuellement contenus dans la structure.
</details>

Les accesseurs suivant permettent d'obtenir ces deux informations (en lecture seulement) :

```cpp
tab.capacity();
tab.size();
tab.empty(); // Permet de directement savoir si la taille est nulle
```

Pour ajouter un élément à la fin d'un tableau, on utilise `push_back`.
Cette fonction membre augmentera la capacité du `std::vector` si celui-ci devient trop petit.

```cpp
tab.size(); // Donne 3
tab.push_back(6); // On insère 6 à la fin du tableau
tab.size(); // Donne 4
```

À l'issu d'un `push_back`, le schéma présenté ci-avant devient le suivant :

<div align="center"><img src="img/vector_b.png" width="400"></div>

Un `push_back` supplémentaire cause l'agrandissement de la capacité :

<div align="center"><img src="img/vector_c.png" width="575"></div>

On peut également retirer le dernier élément avec `pop_back` :

```cpp
tab.size(); // Donne 4
tab.pop_back(); // On retire le dernier élément
tab.size(); // Donne 3
```

:arrow_forward: Dans un nouveau programme, ajoutez une fonction `main`, instanciez-y un `std::vector` et placez-y des éléments. Créez une fonction permettant d'afficher le contenu de la structure.

<details><summary>Tableau de taille fixe</summary>

La structure [`std::array<Type, n>`](https://en.cppreference.com/w/cpp/container/array) permet de manipuler un tableau de `Type` de taille `n` (connue à la compilation).
Un `std::array` aura toujours sa taille égale à sa capacité.
On accède à un élément avec l'opérateur `[]` comme pour le `std::vector`.

```cpp
std::array<long, 12> tab;
tab[7] = 12567;
```

:arrow_forward: Même question que précédemment mais avec un `std::array`.
</details>

### Chaîne de caractères

Les chaînes de caractères sont un tableau particulier qui ne sert qu'à stocker des caractères.
Le type utilisé en C++ est [`std::string`](https://en.cppreference.com/w/cpp/string/basic_string).

```cpp
#include <string>

int main() {
    std::string texte{"Texte"};
}
```

Les chaines de caractères sont concaténables avec l'opérateur `+`.

```cpp
std::string a{"Concaténation de "};
std::string b{"chaînes"};
std::string c{a + b};
```

C'est le seul conteneur permettant de faire des comparaisons simples.

```cpp
std::string a{"MotA"};
std::string b{"MotB"};

if (a == b) {

}
```

Il est aussi possible d'établir une relation d'ordre qui suit l'ordre lexicographique.

```cpp
std::string a{"abc"};
std::string b{"abd"};

bool isAFirst = a < b; // Vrai
```

### Listes chaînées

Les listes chaînées sont des conteneurs non-contigus.
On parcourt les éléments du premier au dernier, chacun connaissant son suivant. 

On utilisera [`std::list`](https://en.cppreference.com/w/cpp/container/list) en C++.

La liste chaînée ressemble à ceci :

<div align="center"><img src="img/list_a.png" width="600"></div>

<details><summary>Liste doublement chaînées</summary>

En réalité, `std::list` permet de se déplacer sur l'élément suivant et l'élément précédent.
Elle est donc *doublement chaînée*.
En mémoire, cela ressemble à ceci :

<div align="center"><img src="img/dlist.png" width="720"></div>

Il existe tout de même une implémentation d'une liste *simplement* chaînée : `std::forward_list`.
Dans le cadre de ce cours, `std::list` sera utilisée en tant que liste chaînée simple.
</details>

La manipulation d'une `std::list` est très proche de celle d'un `std::vector`.

```cpp
std::list<int> l{3, 5, 4, 6}; // Créer une liste de 4 éléments.
l.pop_back(); // Retire le 6 à la fin de la liste.
l.push_back(4); // Insert 4 à la fin de la liste.
```

Ainsi, en appliquant un `push_back` (avec le nombre 6, sur la liste chaînée présentée en schéma ci-avant), on obtient :

<div align="center"><img src="img/list_b.png" width="720"></div>

La mémoire d'un ordinateur en cours d'utilisation est généralement fragmentée.
Cela signifie que si 15 Gio sur 16 Gio sont libres, il se peut qu'allouer 14 Gio avec `std::vector` soit impossible car celui-ci utilise de la mémoire contiguë.
En revanche, une liste chaînée pourrait occuper plus facilement ces 14 Gio de mémoire car les différents éléments n'ont pas de contrainte de contiguité.

Cet avantage est cependant contraint par le fait que chaque élément d'une liste chaînée occupe davantage de mémoire que s'il avait été conservé au sein d'un `std::vector`.
En effet, on doit associer à chaque élément l'adresse de son suivant, ce qui occupe, sur une machine 64 bits (le cas commun à ce jour), 8 octets.
Ainsi, dans le pire cas, cela multiplie par 9 l'espace occupé par chaque donnée (une donnée occupant elle-même seulement 1 octet).
Ainsi, conserver 1 Gio de données requiert 1 Gio pour `std::vector` mais requiert 9 Gio pour une liste chaînée.

<details><summary>Et pour `std::list`...</summary>

Le type `std::list` correspondant en fait à une liste doublement chaînée, il faut ajouter 16 octets à la taille d'un élément plutôt que 8.

</details>

Enfin, nous le verrons à la fin de cette séance, le choix entre `std::vector` et `std::list` sera également déterminé par d'autres critères.

### Dictionnaires

Le dictionnaire est une structure permettant d'associer deux variables sous la forme d'une clé et d'une valeur. 
Ces deux variables pourront être de types différents.
Cependant toutes les clés auront le même type `KeyType` et toutes les valeurs auront le même type `ValueType`.

Les clés d'un dictionnaire doivent être uniques !

En C++, on utilise [`std::map<KeyType, ValueType>`](https://en.cppreference.com/w/cpp/container/map) pour obtenir un dictionnaire. 
Dans l'exemple suivant on définit la variable `dict` comme étant un dictionnaire dont la clé est un réel et la valeur associée est une chaîne de caractères.

```cpp
std::map<float, std::string> dict;
```

On peut ensuite accéder à une valeur de deux manières différentes.

L'accès en lecture seule utilise la fonction membre `at`.
Celle-ci est `const`, elle ne peut donc pas modifier l'instance du dictionnaire.
Si la clé n'est pas présente une erreur est levée.

```cpp
dict.at(4.0f);
```

Le second accès permet de faire de la lecture et de l'écriture.
En essayant de lire la valeur d'une clé non présente, la clé sera ajoutée au dictionnaire et la valeur associée sera construite avec le constructeur par défaut.

```cpp
dict[2.0f] = std::string{"texte"};
```

Pour retirer une entrée dans le dictionnaire, on utilise la fonction membre `erase`.

```cpp
dict.erase(2.0f);
```

Pour parcourir le contenu d'un dictionnaire on utilisera les itérateurs.
Cette syntaxe disponnible en C++17 nécessite l'utilisation de `-std=c++17` ou `-std=c++1z` dans la ligne de commande de `g++`.
On ne demande pas de retenir cette syntaxe, elle vous sera utile pour les exercices suivants.

```cpp
for (auto const & [key, value] : dict) {
    /* Ici, les variables key et value sont définie.
     * Leur type est déterminé automatiquement en fonction des types utilisés pour le dictionnaire.
     * Chaque itération de boucle donnera accès à une paire (clé, valeur).
     */
}
```

:arrow_forward: Utilisez une `std::map<std::string, int>` pour stocker un annuaire. On retrouve le numéro de téléphone d'une personne à partir de son nom. Cet annuaire doit se trouver dans la fonction `main`. Ajoutez les fonctions suivantes (à vous de trouver les bons paramètres et types de retour) :
 - `afficher` : affiche le contenu de l'annuaire (dans quel ordre les informations se trouvent ? );
 - `ajouter` : ajoute un numéro à une personne ;
 - `supprimer` : retire une personne de l'annuaire.
 
:arrow_forward: Que fait votre ajout lorsque vous ajouter un numéro à une personne déjà présente dans l'annuaire ? Ajoutez un message d'averstissement sur la sortie standard indiquant les ancien et nouveau numéros de téléphone.

:arrow_forward: Ajoutez une fonction `rechercher` qui affiche toutes les informations de l'annuaire pour les noms commencant par un préfix passé en paramètre (`rechercher("j")` donne les noms et numéros de toutes les personnes dont le nom commence par `j`). Indice : [algorithm](https://en.cppreference.com/w/cpp/algorithm)

:arrow_forward: Ignorez les majuscules pour la fonction précédante.

<details><summary>Itérateurs</summary>

Un itérateur est un outil en C++ permettant le parcourt d'un conteneur.

On peut donc parcourir un `std::vector` via un itérateur plutôt qu'en utilisant un indice.
Le départ de l'itérateur s'obtient grâce à `std::begin`.
La fin obtenue grâce à `std::end`, représente l'élément suivant le dernier élément.
Ce n'est donc pas un élément valide du conteneur.

On passe l'itérateur au suivant grâce à l'opérateur `++`.
L'itérateur permet d'accéder à la valeur parcourue grâce à l'opérateur `*`.

```cpp
std::vector<char> vec{'g', 'h', 'b', 'a'};
for (auto it = std::begin(vec); it != std::end(vec); ++it) {
    std::cout << (*it) << ' ';
}
```

On peut relire l'exemple suivant de cette manière :

1. Initialisation de `it` dont le type est détecté automatiquement afin de stocker l'itérateur sur le premier élément de `vec`.
2. Tant que `it` n'est pas sorti de `vec`.
3. Affichage de la valeur indiquée par `it`.
4. Déplacement d'`it` sur la case suivante de `vec`.

Cette syntaxe est un peu lourde à écrire et une version allégée a été ajoutée au langage : le [range-for](https://en.cppreference.com/w/cpp/language/range-for).

```cpp
std::vector<char> vec{'g', 'h', 'b', 'a'};
for (auto const & val : vec) {
    std::cout << val << ' ';
}
```

Dans cette version, `val` contient directement une des valeurs.
Il n'y a donc pas besoin d'utiliser l'opérateur `*`.
Comme nous n'avons pas besoin de faire une copie, nous laissons `val` être une référence constante.
`auto` permet de déduire automatiquement le type `char`, `val` est donc de type `char const &`.

Dans le cadre d'une `std::map<KeyType, ValueType>`, chaque élément parcouru est une paire (clé, valeur).
Le type correspondant en C++ est `std::pair<KeyType, ValueType>`.
Les variables membres `first` et `second` permettent d'accéder respectivement à la clé et à la valeur.

```cpp
std::map<std::string, int> dict;

for (std::pair<std::string, int> const & paire : dict) { // Il est possible d'utiliser `auto` pour le type.
    paire.first; // clé
    paire.second; // valeur associée
}
```

<details><summary>Destructuration des paires</summary>

La destructuration des paires permet de séparer le contenu d'une paire dans deux variables distinctes.

```cpp
std::paire<char, int> p{'a', 12};
auto [cle, valeur] = p;
```

Les variables `cle` et `valeur` sont initialisés avec les valeurs présentes dans la paire.

Cette destructuration permet de séparé directement la clé et la valeur lors du parcours d'un dictionnaire avec des itérateurs.

```cpp
std::map<std::string, int> dict;
for (auto const & [cle, valeur] : dict) {
}
```

Ici, la variable `cle` est une référence constante sur une clé (`std::string const &`).
Et la variable `valeur` est une référence constante sur la valeur associée à cette clé (`int const &`).

Pour plus d'informations sur ce sujet : [structure bindings](https://en.cppreference.com/w/cpp/language/structured_binding).
</details>
</details>

## Complexité

La complexité d'un algorithme (par extension, d'une fonction, d'un programme) correspond à l'ordre de grandeur d'une ressource nécessaire à l'exécution de l'algorithme.
Les deux ressources les plus communément étudiées sont le temps et l'espace, nous nous concentrerons sur le temps dans ce cours.

La complexité temporelle se mesure en fonction de la taille des données en entrée et correspond, pour simplifier, au nombre d'opérations à effectuer.
Ainsi, dans un tableau contigu, l'accès à un cellule (que ce soit pour lire ou écrire) est en temps constant quelle que soit la taille du tableau ou la cellule à laquelle on accède.
Au contraire, au sein d'une liste chaînée, l'accès à la cellule d'ordre $`n`$ nécessite autant d'opérations : l'accès à la tête qui permet de déterminer la localisation de la cellule suivante,
elle-même indiquant la localisation de sa suivante, et ainsi de suite.
La complexité d'accès est donc linéaire en fonction de la taille de la liste chaînée.

Pour prendre en compte les simplifications nécessaires qui ont été faites, on utilise une notation particulière pour indiquer la complexité d'un algorithme : $`O(expression)`$.
Formellement, la définition ne nous intéressera pas (pour les curieux, voir https://en.wikipedia.org/wiki/Big_O_notation à la section *Formal definition*).
On se contentera de retenir que l'expression utilisée est une limite maximale jamais atteinte, à un facteur multiplicatif quelconque près.

Par exemple, si l'on considère à nouveau l'accès à un élément d'une liste chaînée : chaque étape coûte en pratique peut-être « une opération » ou « 18 opérations ».
Dans le premier cas, on serait tenté de dire que la complexité est $`O(1 \times n)`$ tandis que dans le second on noterait $`O(18 \times n)`$.
Or, la notation « grand O » prend déjà en compte ce facteur puisqu'il n'apporte pas d'information supplémentaire, et on notera simplement $`O(n)`$, que l'on qualifiera de complexité linéaire.

Remarque : puisque cette notation correspond à une limite supérieure, on pourrait également écrire $`O(n^2)`$ par exemple.
Une contrainte que l'on s'impose est donc d'indiquer l'expression minimale.

### Inventaire réduit des complexités

Les complexité classiques, triées de manière croissante, sont :
- $`O(1)`$, complexité constante ;
- $`O(log(n)`$, complexité logarithmique ;
- $`O(n)`$, complexité linéaire ;
- $`O(n \times log(n))`$, complexité linéarithmique ;
- $`O(n^2)`$, complexité quadratique ;
- $`O(n^3)`$, complexité cubique.

### Exemples

#### Cas du tri naïf

Les complexité de la forme $`O(n^k)`$ sont appelées polynomiales et seul le terme avec le plus grand exposant est noté : les autres sont négligeables lorsque $`n`$ est grand.
Ainsi, considérons une liste de nombres : (5, 2, 4, 1, 3).
Si l'on implémente un algorithme de tri naïf, on peut procéder comme suit :
pour chaque élément de la liste, chercher à partir de celui-ci dans la liste le plus petit élément procéder à un échange de ces deux éléments.
Cela donne :
- (5, 2, 4, 1, 3) (liste initiale) : on part de 5 et on trouve le minimum 1 ;
- (1, 2, 4, 5, 3) : on part alors de 2 et on trouve le minimum 2 ;
- (1, 2, 4, 5, 3) : on part alors de 4 et on trouve le minimum 3 ;
- (1, 2, 3, 5, 4) : on part alors de 5 et on trouve le minimum 4 ;
- (1, 2, 3, 4, 5).

D'une manière générale, trier ainsi une liste comportant $`n`$ éléments demande de procéder à autant de parcours partiels de la liste, chacun de ces parcours partiels étant moins longs de 1 que le précédent.
On trouve donc qu'il faut :

```math
\sum_{i=1}^{n} {i} = \frac{n \times (n+1)}{2} = \frac{1}{2} \times n^2 + \frac{1}{2} \times n
```

et donc, puisque seul le terme de plus grand exposant est important, et que les facteurs constants doivent également être ignorés, on trouve une complexité quadratique, $`O(n^2)`$.
Dans le cadre du traitement d'image, une complexité peut être exprimée selon deux variables, la largeur et la hauteur de l'image, et on peut donc avoir des $`O(w \times h)`$ par exemple.

#### Cas de la recherche dans une liste triée

Afin de donner un autre exemple, considérons à nouveau une liste de nombres, triés : (0, 3, 8, 15, 24, 35, 48, 63, 80, 99, 120, 143, 168).
Si l'on cherche un élément quelconque, on peut parcourir toute la liste :
- si on le trouve, on a terminé et retourne la position correspondante ;
- si on a atteint la fin de la liste, on a terminé et on indique que l'élément recherché n'est pas présent dans celle-ci.

Dans le pire cas, l'élément recherché n'est pas présent, et l'algorithme demande donc $`n`$ opérations, ce qui lui donne une complexité linéaire ($`O(n)`$).
Cependant, on peut profiter du fait que la liste est triée !
En effet, dès lors, on peut procéder différemment on part du milieu de la liste et on compare avec l'élément recherché : si c'est égal, on a terminé, sinon, selon la comparaison, on prend le milieu du début ou le milieu de la fin de la liste et on recommence.
Si on arrive à un point pour lequel on ne peut plus continuer, on arrête et on déclare l'élément recherché absent de la liste.

Prenons deux exemples, d'abord avec $`v=35`$ :
- on débute au milieu avec le nombre 48, on a $`v < 48`$, on doit donc poursuivre avec le début de la liste ((0, 3, 8, 15, 24, 35)) ;
- le nouveau milieu est 15, on a $`v > 15`$, on doit donc poursuivre avec la fin de la liste ((24, 35)) ;
- le nouveau milieu est 35, on a $`v = 35`$ donc on a trouvé la position de 35 dans la liste.

Ensuite avec $`v=100`$ :
- on débute au milieu avec le nombre 48, on a $`v > 48`$, on doit donc poursuivre avec la fin de la liste ((63, 80, 99, 120, 143, 168)) ;
- le nouveau milieu est 120, on a $`v < 120`$, on doit donc poursuivre avec le début de la liste ((63, 80, 99)) ;
- le nouveau milieu est 80, on a $`v > 80`$, on doit donc poursuivre avec la fin de la liste ((99)) ;
- le nouveau milieu est 99, on a $`v > 99`$, on doit donc poursuivre avec la fin de la liste (()) ;
- la liste est vide, 100 est donc absent.

Chaque itération de cet algorithme divise par deux la taille de la liste à traiter, cela veut dire que si l'on a effectué 4 opérations dans le pire cas (avec $`v`$ absent),
on a une liste d'au maximum $`2^4 = 16`$ éléments (ce qui est bien le cas).
Réciproquement, une liste de $`n`$ éléments nécessitera au plus $`log_2(n)`$ opérations.
Cet algorithme (que l'on appelle recherche dichotomique) est donc de complexité logarithmique, notée $`O(log_2(n))`$.

Cela dit, pour cela, nous avons dû accéder aux éléments de la liste en complexité constante, or...

### Complexités en fonction de la structure de données

Nous ne considérerons que deux structures de données : la liste contiguë (`std::vector`, `std::array`, un tableau, …) et la liste chaînée (`std::list`, `std::forward_list`, …).
Pour ces deux structures de données, on doit considérer :
- l'accès aux données (attention, il s'agit d'accès au sein de la structure de données, que ce soit pour lire ou pour écrire) ;
- la modification (à nouveau, il s'agit de modifier **la structure de données**, on parle donc d'ajouter ou supprimer des éléments).

Remarque sur le vocabulaire : on parle d'accès *aléatoire* pour un accès *quelconque* sans préparation ou autre connaissance.
Ce terme est fréquemment employé, un autre exemple est l'acronyme *RAM* pour *Random Access Memory*, une mémoire dont les accès aléatoire sont possibles et efficaces.

#### Liste contiguë

Dans une liste contiguë (schéma ci-dessous), l'accès, que ce soit en tête, en queue ou bien où que ce soit se fait en temps constant : il s'agit d'un simple décalage à partir du début.

<div align="center"><img src="img/vector_c.png" width="575"></div>

En revanche, les modifications peuvent causer des changements : l'insertion d'un élément autre part qu'en fin nécessite un décalage des valeurs conservées.
Par exemple, si l'on désire insérer 8 en troisième position, il faut :
- décaler les valeurs à partir de la troisième position jusqu'à la fin de la liste contiguë ;
- écrire la valeur 8 en troisième position.

Ces étapes correspondent au schémas suivants :

<div align="center"><img src="img/vector_d.png" width="575"></div>
<div align="center"><img src="img/vector_e.png" width="575"></div>

De même, la suppression d'un élément autre part qu'en queue nécessite le décalage des valeurs jusqu'à cet élément.
Par exemple, si l'on désire supprimer la tête de la liste contiguë, on suit le schéma suivant :

<div align="center"><img src="img/vector_f.png" width="575"></div>

#### Liste chaînée

Au sein d'une liste chaînée (schéma ci-dessous), l'accès à un élément autre que la tête n'est pas immédiat.
Atteindre un élément nécessite de connaître son emplacement, lequel est connu par l'élément précédent.
Atteindre le nième élément requiert donc n opérations.

<div align="center"><img src="img/list_b.png" width="720"></div>

En revanche, l'insertion comme la suppression, que ce soit en tête, en queue ou bien où que ce soit, se fait en temps constant.
Pour le premier cas, il suffit de créer un nouvel élément puis de corriger le chaînage.
Pour le second cas, après avoir corrigé le chaînage, il faut libérer l'élément.
Ces deux cas sont illustrés par les schémas ci-après.

Insertion :

<div align="center"><img src="img/list_c.png" width="720"></div>

Suppression :

<div align="center"><img src="img/list_d.png" width="720"></div>

#### Résumé

|                |  accès en tête  |  accès en queue  |  accès aléatoire  |  modification en tête  |  modification en queue  |  modification aléatoire  |
|----------------|-----------------|------------------|-------------------|------------------------|-------------------------|--------------------------|
| liste contiguë | $`O(1)`$        | $`O(1)`$         | $`O(1)`$          | $`O(n)`$               | $`O(1)`$                | $`O(n)`$                 |
| liste chaînée  | $`O(1)`$        | $`O(n)`$         | $`O(n)`$          | $`O(1)`$               | $`O(1)`$                | $`O(1)`$                 |

Quelques remarques :
- la modification en queue au sein d'une liste contiguë pouvant causer une réallocation, et donc nécessiter une recopie des données, elle pourrait être $`O(n)`$, cependant, si la structure de données est bien implémentée, cela arrive rarement, on parle alors de complexité constante *amortie* ;
- l'accès à un élément aléatoire de la liste chaînée peut également être amorti en $`O(1)`$ si les accès sont nécessairement fait dans l'ordre de la liste chaînée ;
- les modifications en queue et aléatoires d'une liste chaînée sont considérées dans le cas où l'on connaît déjà la position d'insertion/suppression : le coût de recherche, le cas échéant, étant déjà couvert par l'opération d'accès.

### Conclusion sur l'exemple de recherche dichotomique

Implémenter la recherche dichotomique sur une liste chaînée serait une mauvaise idée.
En effet, atteindre le milieu de la liste nécessite systématiquement un accès aléatoire en $`O(n)`$.
Si l'on souhaite calculer précisément, on devra tenir compte de la décroissance exponentielle de la taille des listes traitées à chaque itération.
On trouvera finalement que la recherche dichotomique, si elle est bien de complexité $`O(log_2(n))`$ appliquée sur un `std::vector`, sera
malheureusement de complexité $`O(n \times log_2(n))`$ lorsqu'elle est appliquée sur un `std::list`.
